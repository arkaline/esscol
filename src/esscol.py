# -*- coding: utf-8 -*-
# see http://aoo-extensions.sourceforge.net/en/project/watchingwindow
import uno
import unohelper

IMPL_NAME = "lo.org.esscol.esscol"
RESOURCE_NAME = "private:resource/toolpanel/lo.org.esscol/esscol"
CONFIG_NODE = "/lo.org.esscol.esscol/Settings"

def create_uno_struct(cTypeName):
    """Create a UNO struct and return it.
    Similar to the function of the same name in OOo Basic. -- Copied from Danny Brewer library
    """
    sm = uno.getComponentContext().ServiceManager
    oCoreReflection = sm.createInstance( "com.sun.star.reflection.CoreReflection" )
    # Get the IDL class for the type name
    oXIdlClass = oCoreReflection.forName( cTypeName )
    # Create the struct.
    __oReturnValue, oStruct = oXIdlClass.createObject( None )
    return oStruct

def create_control(ctx, control_type, x, y, width, height, names, values):
    """ create a control. """
    smgr = ctx.getServiceManager()
    
    ctrl = smgr.createInstanceWithContext("com.sun.star.awt." + control_type, ctx)
    ctrl_model = smgr.createInstanceWithContext("com.sun.star.awt." + control_type + "Model", ctx)
    
    if len(names) > 0:
        ctrl_model.setPropertyValues(names, values)
    ctrl.setModel(ctrl_model)
    ctrl.setPosSize(x, y, width, height, PS_POSSIZE)
    return ctrl


def create_container(ctx, parent, names, values, __fit=True):
    """ create control container. """
    cont = create_control(ctx, "UnoControlContainer", 0, 0, 0, 0, names, values)
    cont.createPeer(parent.getToolkit(), parent)
    #if fit:
    #    cont.setPosSize()
    return cont

def get_backgroundcolor(window):
    """ Get background color through accesibility api. """
    try:
        return window.getAccessibleContext().getBackground()
    except:
        pass
    return 0xeeeeee

"""

"""
import threading

from com.sun.star.awt import (XWindowListener, XActionListener, XMouseListener)
from com.sun.star.ui import XUIElement, XToolPanel
from com.sun.star.awt.PosSize import (POSSIZE as PS_POSSIZE)
from com.sun.star.ui.UIElementType import TOOLPANEL as UET_TOOLPANEL
from com.sun.star.lang import XComponent

class EsscolModel(unohelper.Base, XUIElement, XToolPanel, XComponent):
    """ Esscol model. """
    
    def __init__(self, ctx, frame, parent):
        self.ctx = ctx
        self.frame = frame
        self.parent = parent
        
        self.view = None
        self.window = None
        try:
            view = EsscolView(ctx, self, frame, parent)
            self.view = view
            self.window = view.cont
            
            def _focus_back():
                self.frame.getContainerWindow().setFocus()
            
            threading.Timer(0.3, _focus_back).start()
        except Exception as e:
            print(e)
    
    # XComponent
    def dispose(self):
        self.ctx = None
        self.frame = None
        self.parent = None
        self.view = None
        self.window = None
    
    def addEventListener(self, ev): pass
    def removeEventListener(self, ev): pass
    
    # XUIElement
    def getRealInterface(self):
        return self
    @property
    def Frame(self):
        return self.frame
    @property
    def ResourceURL(self):
        return RESOURCE_NAME
    @property
    def Type(self):
        return UET_TOOLPANEL
    
    # XToolPanel
    def createAccessible(self, __parent):
        return self.window.getAccessibleContext()
    @property
    def Window(self):
        return self.window
    
    def dispatch(self, cmd, args):
        """ dispatch with arguments. """
        helper = self.ctx.getServiceManager().createInstanceWithContext("com.sun.star.frame.DispatchHelper", self.ctx)
        helper.executeDispatch(self.frame, cmd, "_self", 0, args)
    
    def hidden(self):
        self.data_model.enable_update(False)
    def shown(self):
        self.data_model.enable_update(True)

from com.sun.star.awt.MouseButton import LEFT as MB_LEFT

class EsscolView(unohelper.Base, XWindowListener, XActionListener, XMouseListener):
    """ Esscol view. """
    __couleurs__ = ["rouge","vert","bleu","orange","cyan","jaune","rose"]
    codeCouleur = {'noir':0X00000000, 'bleu':0X000000ff, 'vert':0X0000ff00, 'rouge':0X00ff0000, 'jaune':0X00ffff00, 'cyan':0X0000ffff, 'rose':0X00ff00ff, 'orange':0X00ffcc00}
    codePastel = {'noir':0X00c6c6c6, 'bleu':0X00c6c6ff, 'vert':0X00c6ffc6, 'rouge':0X00ffc6c6, 'jaune':0X00ffffc6, 'cyan':0X00c6ffff, 'rose':0X00ffc6ff, 'orange':0X00ffe1c6}
    LR_MARGIN = 3
    TB_MARGIN = 3
    BUTTON_SEP = 2
    BUTTON_SZ = 50
    
    def __init__(self, ctx, model, frame, parent):
        self.model = model
        self.parent = parent
        self.controller = frame.getController()
        self.ctx = ctx
        self.width = 200
        self.height = 500
        self.lbtn = []
        self.configBtn = None
        
        try:
            self._create_view(ctx, parent)
        except Exception as e:
            print(("Failed to create Esscol view: %s" % e))
        parent.addWindowListener(self)
    
    def _create_view(self, ctx, parent):
        LR_MARGIN = self.LR_MARGIN
        TB_MARGIN = self.TB_MARGIN
        BUTTON_SEP = self.BUTTON_SEP
        
        cont = create_container(ctx, parent, ("BackgroundColor",), (get_backgroundcolor(parent),))
        self.cont = cont
        smgr = ctx.getServiceManager()
        self.smgr = smgr
        self.ctx = ctx
        
        settings= Settings()
        
        # création des boutons pour les fonctions sélectionnées
        lcols = settings.get("__couleurs__")
        lcols.append("noir")
        for col in ["noir","rouge","vert","bleu","orange","cyan","jaune","rose"]:
            for fct in ['encadre','souligne','surligne','colorie']:
                btn = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlImageControl', ctx)        
                btn_model = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlImageControlModel', ctx)
                img = "vnd.sun.star.extension://"+IMPL_NAME+"/images/"+fct+col+".png"
                if fct == 'colorie' and col == 'noir':
                    btn_model.setPropertyValues( ('ImageURL','ScaleImage','ScaleMode','HelpText','Name',),
                                (img,True,1,'efface formatage','effaceur',) )
                else:
                    btn_model.setPropertyValues( ('ImageURL','ScaleImage','ScaleMode','HelpText','Name',),
                                (img,True,1,fct+' '+col,fct+'_'+col,) )
                btn.setModel(btn_model)
                cont.addControl(fct, btn)
                btn.addMouseListener(self)
                if col in lcols:
                    btn.setVisible(True)
                else:
                    btn.setVisible(False)
                self.lbtn.append(btn)
        
        self.configBtn = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlImageControl', ctx)        
        btn_model = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlImageControlModel', ctx)
        img = "vnd.sun.star.extension://"+IMPL_NAME+"/images/config.png"
        btn_model.setPropertyValues( ('ImageURL','ScaleImage','ScaleMode','HelpText','Name',),
                        (img,True,1,'configuration ESScol','configuration',) )
        self.configBtn.setModel(btn_model)
        cont.addControl(fct, self.configBtn)
        self.configBtn.addMouseListener(self)
        
        self.__update_view()
        
    def __update_view(self):
        LR_MARGIN = self.LR_MARGIN
        TB_MARGIN = self.TB_MARGIN
        BUTTON_SEP = self.BUTTON_SEP
        
        settings = Settings()

        # replacement des boutons si nécessaire
        BUTTON_SZ = self.BUTTON_SZ
        posX = LR_MARGIN
        posY = TB_MARGIN
        lcols = settings.get("__couleurs__")
        lcols.append("noir")
        i = 0
        for col in ["noir","rouge","vert","bleu","orange","cyan","jaune","rose"]:
            for fct in ['encadre','souligne','surligne','colorie']:
                if col in lcols:
                    self.lbtn[i].setPosSize(posX, posY, BUTTON_SZ, BUTTON_SZ, PS_POSSIZE)
                    posX += (BUTTON_SZ+BUTTON_SEP)
                    self.lbtn[i].setVisible(True)
                else:
                    self.lbtn[i].setVisible(False)
                i += 1
            if col in lcols:
                posY += (BUTTON_SZ+BUTTON_SEP)
            posX = LR_MARGIN
        self.configBtn.setPosSize(posX, posY, BUTTON_SZ, BUTTON_SZ, PS_POSSIZE)

    # XEventListener
    def disposing(self, __ev):
        self.cont = None
        self.model = None
    
    # XWindowListener
    def windowHidden(self, ev): pass
    def windowShown(self, ev): pass
    def windowMoved(self, ev): pass
    def windowResized(self, ev): pass
    
    # XMouseListener
    def mouseEntered(self, ev):
        btn_model = ev.Source.Model
        btn_model.setPropertyValues( ('BackgroundColor',), (0xffffff,) )
    def mouseExited(self, ev):
        btn_model = ev.Source.Model
        btn_model.setPropertyValues( ('BackgroundColor',), (get_backgroundcolor(self.parent),) )
    def mousePressed(self, ev):
        if ev.Buttons == MB_LEFT and ev.ClickCount == 1:
            desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
            xDocument = desktop.getCurrentComponent()
            try:
                xTextRange = get_text_range(xDocument)
                if xTextRange == None:
                    return False
                fcol = ev.Source.Model.HelpText.split(' ')
                if fcol[0] == "encadre":
                    for cursor in xTextRange:
                        self.encadre(cursor, EsscolView.codeCouleur[fcol[1]])
                elif fcol[0] == "surligne":
                    for cursor in xTextRange:
                        self.surligne(cursor, EsscolView.codePastel[fcol[1]])
                elif fcol[0] == "souligne":
                    for cursor in xTextRange:
                        self.souligne(cursor, EsscolView.codeCouleur[fcol[1]])
                elif fcol[0] == "efface":
                    for cursor in xTextRange:
                        self.efface(cursor)
                elif fcol[0] == "colorie":
                    for cursor in xTextRange:
                        self.colorie(cursor, EsscolView.codeCouleur[fcol[1]])
                else:
                    self.configure()
    
                del xTextRange
            except:
                pass

    def mouseReleased(self, ev): pass

    # XActionListener
    def actionPerformed(self, __ev):
        try:
            pass
        except Exception as e:
            print(e)

    def configure(self):
        settings = Settings()
        
        smgr = uno.getComponentContext().ServiceManager
        dp = smgr.createInstance("com.sun.star.awt.DialogProvider")
        dlg = dp.createDialog("vnd.sun.star.extension://"+IMPL_NAME+"/ESScolDialog.xdl")
        dlg.getControl("TextField1").setText(str(settings.get("__largeur_bordures__")))
        choix = {"Simple":1, "Gras":12, "Double":2, "Pointillé":3, "Tiret":5, "Tiret long":6, "Ondulation":10, "Pointillé (gras)":13, "Tiret (gras)":14, "Tiret long (gras)":15, "Ondulation":18}
        rev_choix = dict([(choix[k], k) for k in choix.keys()])
        dlg.getControl("ComboBox1").addItems([k for k in choix.keys()], 0)
        dlg.getControl("ComboBox1").setText(rev_choix[settings.get("__soulignage__")])
        select = dict([(col,0) for col in EsscolView.__couleurs__])
        for col in settings.get("__couleurs__"):
            select[col] = 1
        for col in select.keys():
            try:
                dlg.getControl("Check_"+col).setState(select[col])
            except:
                pass
        if dlg.execute():
            try:
                settings.setValue("__largeur_bordures__", int(dlg.getControl("TextField1").getText()))
            except:
                settings.setValue("__largeur_bordures__", 50)
            try:
                settings.setValue("__soulignage__", choix[dlg.getControl("ComboBox1").getText()])
            except:
                settings.setValue("__soulignage__", 1)
            lcols = []
            for col in select.keys():
                try:
                    if dlg.getControl("Check_"+col).getState():
                        lcols.append(col)
                except:
                    pass
            try:
                settings.setValue("__couleurs__", lcols)
            except:
                settings.setValue("__couleurs__", ["rouge","vert","bleu","jaune"])
                
            self.__update_view()

    def encadre(self, cursor, couleur):
        # https://www.openoffice.org/api/docs/common/ref/com/sun/star/style/CharacterProperties.html
        settings = Settings()
        
        ligne = uno.createUnoStruct("com.sun.star.table.BorderLine")
        ligne.Color = couleur
        ligne.InnerLineWidth = 0
        ligne.OuterLineWidth = settings.get("__largeur_bordures__")
        ligne.LineDistance = 0

        cursor.setPropertyValue("CharTopBorder", ligne)
        cursor.setPropertyValue("CharBottomBorder", ligne)
        cursor.setPropertyValue("CharLeftBorder", ligne)
        cursor.setPropertyValue("CharRightBorder", ligne)

    def surligne(self, cursor, couleur):
        # https://www.openoffice.org/api/docs/common/ref/com/sun/star/style/CharacterProperties.html
        cursor.setPropertyValue("CharBackTransparent", True)
        cursor.setPropertyValue("CharBackColor", couleur)

    def colorie(self, cursor, couleur):
        # https://www.openoffice.org/api/docs/common/ref/com/sun/star/style/CharacterProperties.html
        cursor.setPropertyValue("CharColor", couleur)

    def souligne(self, cursor, couleur):
        # https://www.openoffice.org/api/docs/common/ref/com/sun/star/style/CharacterProperties.html
        settings = Settings()
        
        cursor.setPropertyValue("CharUnderlineHasColor", True)
        cursor.setPropertyValue("CharUnderlineColor", couleur)
        cursor.setPropertyValue("CharUnderline", settings.get("__soulignage__"))

    def efface(self, cursor):
        # https://www.openoffice.org/api/docs/common/ref/com/sun/star/style/CharacterProperties.html
        cursor.setPropertyToDefault("CharUnderlineHasColor")
        cursor.setPropertyToDefault("CharUnderline")
        cursor.setPropertyToDefault("CharBackTransparent")
        cursor.setPropertyToDefault("CharBackColor")
        cursor.setPropertyToDefault("CharTopBorder")
        cursor.setPropertyToDefault("CharBottomBorder")
        cursor.setPropertyToDefault("CharLeftBorder")
        cursor.setPropertyToDefault("CharRightBorder")
        cursor.setPropertyToDefault("CharColor")

"""

"""
from com.sun.star.ui import XUIElementFactory
class EsscolFactory(unohelper.Base, XUIElementFactory):
    """ Factory for Esscol """
    def __init__(self, ctx):
        self.ctx = ctx
    
    # XUIElementFactory
    def createUIElement(self, name, args):
        element = None
        if name == RESOURCE_NAME:
            frame = None
            parent = None
            for arg in args:
                if arg.Name == "Frame":
                    frame = arg.Value
                elif arg.Name == "ParentWindow":
                    parent = arg.Value
            if frame and parent:
                try:
                    element = EsscolModel(self.ctx, frame, parent)
                except Exception as e:
                    print(e)
        return element
    
    # XServiceInfo
    def getImplementationName(self):
        return IMPL_NAME
    def supportsService(self, name):
        return IMPL_NAME == name
    def supportedServiceNames(self):
        return (IMPL_NAME,)

g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(
    EsscolFactory, IMPL_NAME, (IMPL_NAME,),)

"""

"""
def get_text_range(xDocument):
    """
        Récupère le textRange correspondant au mot sous le curseur ou à la sélection
    """

    if not xDocument.supportsService("com.sun.star.text.TextDocument"):
        return None

    #the writer controller impl supports the css.view.XSelectionSupplier interface
    xSelectionSupplier = xDocument.getCurrentController()
    xIndexAccess = xSelectionSupplier.getSelection()

    if xIndexAccess.supportsService("com.sun.star.text.TextTableCursor"):
        ##return getXCellTextRange(xDocument, xIndexAccess)
        return None

    xTextRanges = []
    count = 0
    try:
        count = xIndexAccess.getCount()
    except:
        return None

    for i in range(count):
        xTextRange = xIndexAccess.getByIndex(i)
        xText = xTextRange.getText() ## get the XText interface
        xCursor = xText.createTextCursorByRange(xTextRange)
        if len(xCursor.getString()) == 0:
            xCursor.gotoStartOfWord(True)
            xCursor.collapseToStart()
            xCursor.gotoEndOfWord(True)
        xTextRanges.append(xCursor)
    return xTextRanges

"""
    Load and set configuration values.
"""
class Settings(object):
    _loaded = False
    __largeur_bordures__ = 50
    __soulignage__ = 1
    """ Load and set configuration values. """
    def __init__(self, ctx=None):
        try:
            self.ctx = ctx
            if self.ctx is None:
                self.ctx = uno.getComponentContext()
        except:
            pass
    
    def getPropertyValue(self, cua, name):
        try:
            return cua.getPropertyValue(name)
        except:
            pass
        return ""
    
    def _load(self):
        cua = get_config_access(self.ctx, CONFIG_NODE)
        if cua is None:
            return

        Settings.__largeur_bordures__ = self.getPropertyValue(cua, "__largeur_bordures__")
        Settings.__soulignage__ = self.getPropertyValue(cua, "__soulignage__")
        Settings.__couleurs__ = self.getPropertyValue(cua, "__couleurs__").split(";")
        Settings._loaded = True
    
    def get(self, name):
        """ get specified value. """
        if not Settings._loaded:
            self._load()
        if name == "__largeur_bordures__":
            return Settings.__largeur_bordures__
        if name == "__soulignage__":
            return Settings.__soulignage__
        if name == "__couleurs__":
            return Settings.__couleurs__
        return 0

    def setValue(self, name, value):
        """ set specified value. """
        cua = get_config_access(self.ctx, CONFIG_NODE, True)
        if cua is None:
            return
        try:
            if name == "__largeur_bordures__":
                Settings.__largeur_bordures__ = value
                cua.setPropertyValues(("__largeur_bordures__",), (value,))
            elif name == "__soulignage__":
                Settings.__soulignage__ = value
                cua.setPropertyValues(("__soulignage__",), (value,))
            elif name == "__couleurs__":
                Settings.__couleurs__ = value
                cua.setPropertyValues(("__couleurs__",), (";".join(value),))
            cua.commitChanges()
        except:
            pass

from com.sun.star.beans import PropertyValue
from com.sun.star.beans.PropertyState import DIRECT_VALUE as PS_DIRECT_VALUE
def get_config_access(ctx, nodepath, updatable=False):
    """ get configuration access. """
    try:
        arg = PropertyValue("nodepath", 0, nodepath, PS_DIRECT_VALUE)
        cp = ctx.getServiceManager().createInstanceWithContext(
            "com.sun.star.configuration.ConfigurationProvider", ctx)
        if updatable:
            return cp.createInstanceWithArguments(
            "com.sun.star.configuration.ConfigurationUpdateAccess", (arg,))
        else:
            return cp.createInstanceWithArguments(
            "com.sun.star.configuration.ConfigurationAccess", (arg,))
    except:
        return None

"""

"""
if __name__ == "__main__":
    # get the uno component context from the PyUNO runtime
    localContext = uno.getComponentContext()

    # create the UnoUrlResolver
    resolver = localContext.ServiceManager.createInstanceWithContext(
                "com.sun.star.bridge.UnoUrlResolver", localContext )

    # connect to the running office
    ctx = resolver.resolve( "uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext" )
    smgr = ctx.ServiceManager

    # get the central desktop object
    desktop = smgr.createInstanceWithContext( "com.sun.star.frame.Desktop",ctx)

    # access the current writer document
    xDocument = desktop.getCurrentComponent()
    
    ef = EsscolView(xDocument, "encadre rouge")
